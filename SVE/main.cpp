#include <iostream>
#include <string>
#include <fstream>
#include <windows.h>
#include <regex>
#include <vector>
#include <time.h>
#include <algorithm>
#include <stdio.h>
#include <cstring>
#include <stdio.h>
//
#include <sstream>
#include <cstdlib>
#include <ctime>
//

using namespace std;


bool estLisible(const string & nomDuFichier)  
{  
	ifstream fichier(nomDuFichier.c_str());  
	return !fichier.fail();
}

bool estExistant(string nomDuFichier) 
{ 
	using std::cout;
	if (estLisible(nomDuFichier))  
	{  
		//existant
		return true;
	}  
	else  
	{  
		//inexistant
		return false;
	}  
}

/*string chercher(string ligne, string prefix)
{
std::string s (ligne);
std::smatch resultat;
std::regex parametreDeRecherche ("(" + prefix + ")([abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_]*)");

while (std::regex_search (s,resultat,parametreDeRecherche)) {
for (auto x:resultat) cout << x << " ";
std::cout << std::endl;
s = resultat.suffix().str();
}
}*/

string genAleatoire(int nombreCaractere)
{
	string charListe("abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"), aleatoire("");
	for(int i = 0; i < nombreCaractere; i++)
	{
		size_t pos = rand()%60;
		aleatoire+=charListe[pos];
	}

	return aleatoire;
}

bool trier(const std::string& lhs, const std::string& rhs)
{
	return lhs.size() > rhs.size() || (lhs.size() == rhs.size() && lhs > rhs);
}



int main()
{
	SetConsoleTitle(TEXT ("Script's Variables Encoder (SVE) v.0.1 by Ticki84"));
	srand(time(NULL));
	string nomDuFichier, prefix, nomDuFichierOld;
	cout << "Write the name of the file: ";
	cin >> nomDuFichier;
	cin.ignore();
	cout << "Write the prefix of variables: ";
	cin >> prefix;
	cin.ignore();

	string extension (".sqf"), fichierString;
	size_t found = nomDuFichier.find(extension);

	if (found!=string::npos) //On regarde si il y a l'extension .sqf
	{}
	else
	{
		nomDuFichier = nomDuFichier + ".sqf"; //On rajoute � la cha�ne .sqf
	}

	bool existant = estExistant(nomDuFichier);
	if (existant) //On regarde si le fichier existe
	{
		ifstream fichier(nomDuFichier); 

		if(fichier)
		{
			string ligne;
			vector<string> conteneurFichier, nomDesVariables;

			while(getline(fichier, ligne)) //Tant que l'on est pas � la fin du fichier
			{
				conteneurFichier.push_back(ligne); //On met le fichier entier dans un tableau
			}
			fichier.close();
			cout << "File loaded." << endl;

			int const nombreDeLignes(conteneurFichier.size());

			for(int i=0; i<nombreDeLignes; i++) //On cherche ligne par ligne les variables
			{
				string chercherDansLigne (conteneurFichier[i]);
				smatch resultat;
				regex parametreDeRecherche ("(" + prefix + ")([a-zA-Z0-9_]*)");

				while (regex_search (chercherDansLigne,resultat,parametreDeRecherche))
				{
					bool nomVariableDejaExistant(false);
					for(int j = 0; j < static_cast<int>(nomDesVariables.size()); j++) //On regarde si il existe d�j� la valeur dans le tableau
					{
						if(nomDesVariables[j] == resultat[0])
						{
							nomVariableDejaExistant = true;
						}
					}
					if(!nomVariableDejaExistant) //Si la variable n'est pas encore dans le tableau, on l'ajoute
					{
						nomDesVariables.push_back(resultat[0]);
					}
					chercherDansLigne = resultat.suffix().str();
				}
			}
			int const nombreDeVariables(nomDesVariables.size());

			cout << nombreDeVariables << " variables found." << endl;

			sort (nomDesVariables.begin(), nomDesVariables.end(), trier); //On trie le tableau de mani�re � avoir les plus grandes variables en premi�res

			string prefixAleatoire(genAleatoire(5));

			for(int i=0; i<nombreDeVariables; i++)
			{
				string aleatoire(genAleatoire(10));

				for(int j=0; j<nombreDeLignes; j++)
				{
					string tempConteneur;
					regex parametreDeRecherche2 ("(" + nomDesVariables[i] + ")([a-zA-Z0-9_]*)");
					regex_replace (back_inserter(tempConteneur), conteneurFichier[j].begin(), conteneurFichier[j].end(), parametreDeRecherche2, prefixAleatoire + "_" + aleatoire);
					conteneurFichier[j] = tempConteneur;
				}
			}

			nomDuFichierOld = nomDuFichier;
			nomDuFichierOld.erase(nomDuFichierOld.end()-4, nomDuFichierOld.end());
			string nouveauNomDuFichier(nomDuFichierOld + "_old.sqf");

			char* ancienNom(const_cast<char*>(nomDuFichier.c_str()));
			char* nouveauNom(const_cast<char*>(nouveauNomDuFichier.c_str()));

			int renommerFichier;
			renommerFichier = rename(ancienNom , nouveauNom);
			if (renommerFichier == 0)
			{
				cout << "Backup done." << endl;
				ofstream fichierEcriture(nomDuFichier, ios::out | ios::trunc);

				if(fichierEcriture)
				{
					for(int i=0; i<nombreDeLignes; i++)
					{
						fichierEcriture << conteneurFichier[i] << endl;
					}
					fichier.close();
					cout << "Variables encoded!" << endl;
				}
				else
				{
					cout << "File isn't writable." << endl;
				}
			}
			else
			{
				cout << "Error when creating a backup." << endl;
			}
		}
	}
	else
	{
		cout << "File not found." << endl;
	}

	system("pause");

	return 0;
}